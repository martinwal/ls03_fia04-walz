import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class GUI_nachbau extends JFrame {

	private JPanel pnlContent;
	private JTextField txtLabelText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_nachbau frame = new GUI_nachbau();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_nachbau() {
		setTitle("GUI Nachbau: LS03-02-1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 396, 641);
		pnlContent = new JPanel();
		pnlContent.setBackground(UIManager.getColor("menu"));
		pnlContent.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlContent);

		JLabel lblTarget = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblTarget.setBounds(10, 11, 363, 78);
		lblTarget.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setBounds(10, 100, 363, 14);

		JButton btnRedBack = new JButton("Rot");
		btnRedBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlContent.setBackground(Color.red);
			}
		});
		btnRedBack.setBounds(10, 120, 115, 23);

		JButton btnYellowBack = new JButton("Gelb");
		btnYellowBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlContent.setBackground(Color.yellow);
			}
		});
		btnYellowBack.setBounds(10, 154, 115, 23);

		JButton btnBlueBack = new JButton("Blau");
		btnBlueBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlContent.setBackground(Color.blue);
			}
		});
		btnBlueBack.setBounds(258, 120, 115, 23);

		JButton btnDefaultColorBack = new JButton("Standardfarbe");
		btnDefaultColorBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlContent.setBackground(UIManager.getColor("menu"));
			}
		});
		btnDefaultColorBack.setBounds(135, 154, 115, 23);

		JButton btnGreenBack = new JButton("Gr\u00FCn");
		btnGreenBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlContent.setBackground(Color.green);
			}
		});
		btnGreenBack.setBounds(135, 120, 115, 23);

		JButton btnChooseColorBack = new JButton("Farbe w\u00E4hlen");
		btnChooseColorBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color c = JColorChooser.showDialog(lblTarget, "Neue Farbe w�hlen", Color.white);
				pnlContent.setBackground(c);
			}
		});
		btnChooseColorBack.setBounds(258, 154, 115, 23);
		pnlContent.setLayout(null);
		pnlContent.add(lblTarget);
		pnlContent.add(lblAufgabe1);
		pnlContent.add(btnRedBack);
		pnlContent.add(btnYellowBack);
		pnlContent.add(btnBlueBack);
		pnlContent.add(btnDefaultColorBack);
		pnlContent.add(btnGreenBack);
		pnlContent.add(btnChooseColorBack);

		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 191, 360, 14);
		pnlContent.add(lblAufgabe2);

		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblTarget.setFont(new Font("Arial", Font.PLAIN, lblTarget.getFont().getSize()));
			}
		});
		btnArial.setBounds(10, 216, 115, 23);
		pnlContent.add(btnArial);

		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setFont(new Font("Comic Sans MS", Font.PLAIN, lblTarget.getFont().getSize()));
			}
		});
		btnComicSansMs.setBounds(135, 216, 115, 23);
		pnlContent.add(btnComicSansMs);

		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setFont(new Font("Courier New", Font.PLAIN, lblTarget.getFont().getSize()));
			}
		});
		btnCourierNew.setBounds(258, 216, 115, 23);
		pnlContent.add(btnCourierNew);

		txtLabelText = new JTextField();
		txtLabelText.setText("Hier bitte Text eingeben");
		txtLabelText.setBounds(10, 250, 360, 20);
		pnlContent.add(txtLabelText);
		txtLabelText.setColumns(10);

		JButton btnInsLabel = new JButton("Ins Label schreiben");
		btnInsLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setText(txtLabelText.getText());
			}
		});
		btnInsLabel.setBounds(10, 281, 177, 23);
		pnlContent.add(btnInsLabel);

		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setText("");
			}
		});
		btnTextImLabel.setBounds(193, 281, 177, 23);
		pnlContent.add(btnTextImLabel);

		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(10, 315, 360, 14);
		pnlContent.add(lblAufgabe3);

		JButton btnBlackFont = new JButton("Schwarz");
		btnBlackFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setForeground(Color.black);
			}
		});
		btnBlackFont.setBounds(258, 340, 112, 23);
		pnlContent.add(btnBlackFont);

		JButton btnBlueFont = new JButton("Blau");
		btnBlueFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setForeground(Color.blue);
			}
		});
		btnBlueFont.setBounds(135, 340, 115, 23);
		pnlContent.add(btnBlueFont);

		JButton btnRedFont = new JButton("Rot");
		btnRedFont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setForeground(Color.red);
			}
		});
		btnRedFont.setBounds(10, 340, 115, 23);
		pnlContent.add(btnRedFont);

		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblAufgabe4.setBounds(10, 374, 269, 14);
		pnlContent.add(lblAufgabe4);

		JButton btnFontSizeMinus = new JButton("-");
		btnFontSizeMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentFont = lblTarget.getFont().getFontName();
				int size = lblTarget.getFont().getSize();
				if (size > 1)
					size--;
				lblTarget.setFont(new Font(lblTarget.getFont().getFontName(), 0, size));
			}
		});
		btnFontSizeMinus.setBounds(196, 391, 174, 23);
		pnlContent.add(btnFontSizeMinus);

		JButton btnFontSizePlus = new JButton("+");
		btnFontSizePlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setFont(new Font(lblTarget.getFont().getFontName(), 0, lblTarget.getFont().getSize() + 1));
			}
		});

		btnFontSizePlus.setBounds(10, 391, 177, 23);
		pnlContent.add(btnFontSizePlus);

		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setBounds(10, 425, 177, 14);
		pnlContent.add(lblAufgabe5);

		JButton btnLeftAlign = new JButton("linksb\u00FCndig");
		btnLeftAlign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLeftAlign.setBounds(10, 450, 115, 23);
		pnlContent.add(btnLeftAlign);

		JButton btnCenterAlign = new JButton("zentriert");
		btnCenterAlign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnCenterAlign.setBounds(135, 450, 115, 23);
		pnlContent.add(btnCenterAlign);

		JButton btnRightAlign = new JButton("rechtsb\u00FCndig");
		btnRightAlign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTarget.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRightAlign.setBounds(258, 450, 112, 23);
		pnlContent.add(btnRightAlign);

		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 8: Programm beenden");
		lblAufgabeProgramm.setBounds(10, 485, 194, 14);
		pnlContent.add(lblAufgabeProgramm);

		JButton btnNewButton = new JButton("EXIT");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnNewButton.setBounds(10, 507, 360, 95);
		pnlContent.add(btnNewButton);
	}
}
