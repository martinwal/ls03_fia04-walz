package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.LevelController;
import controller.AlienDefenceController;
import controller.GameController;
import model.Level;
import model.User;
import view.game.GameGUI;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.border.MatteBorder;
import javax.swing.UIManager;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow, User user,
			String sourceID) {
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		setLayout(new BorderLayout());

		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		setBounds(0, 0, (int)width, (int)height);
		this.setBackground(Color.BLACK);
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});

		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSpielen_Clicked(alienDefenceController, user);
			}
		});
		pnlButtons.add(btnSpielen);
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setViewportBorder(new MatteBorder(2, 2, 2, 2, (Color) new Color(0, 0, 0)));
		spnLevels.setBackground(Color.black);
		spnLevels.setOpaque(true);
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setBorder(null);
		tblLevels.setColumnSelectionAllowed(true);
		tblLevels.setFillsViewportHeight(true);
		tblLevels.getTableHeader().setBackground(Color.black);
		tblLevels.getTableHeader().setForeground(Color.orange);

		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setForeground(Color.ORANGE);
		this.updateTableData();
		if (sourceID.equals("Testen")) {
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
			btnNewLevel.setVisible(false);
		} else if (sourceID.equals("Editor")) {
			btnSpielen.setVisible(false);
		}
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}

	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController, User user) {
		// Level_id des selektierten Elements auslesen
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));

		// gewähltes Level aus der Persistenz holen
		Level level = alienDefenceController.getLevelController().readLevel(level_id);

		// Gameprozess starten
		Thread t = new Thread("GameThread") {

			@Override
			public void run() {

				// Spielaufruf durchführen
				GameController gameController = alienDefenceController.startGame(level, user);
				new GameGUI(gameController).start();

			}
		};
		// Prozess starten
		t.start();
		// Levelauswahlfenster schließen
		this.leveldesignWindow.dispose();
	}

}
