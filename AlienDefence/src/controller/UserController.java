package controller;

import model.User;
import model.persistance.IUserPersistance;
import model.persistanceDB.UserDB;

/**
 * controller for users
 * 
 * @author Martin Walz TODO implement this class
 */
public class UserController {

	private IUserPersistance userPersistance;

	private UserDB userDB;

	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}

	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * 
	 * @param username
	 *            eindeutige Loginname
	 * @param passwort
	 *            das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		User user = this.userPersistance.readUser(username);
		if (user != null)
			return user;
		else
			return null;
	}
	
	/**
	 * Erstellt einen Nutzer und schreibt diesen in die Datenbank
	 * @param user Der Nutzer der in die Datenbank geschrieben werden soll
	 */
	public void createUser(User user) {
		if(this.userPersistance.readUser(user.getLoginname()) == null) {
			this.userPersistance.createUser(user);
		}
	}
	
	/**
	 * Update den angegebenen Benutzer in der Datenbank
	 * @param user Der ge�nderte Nutzer
	 */
	public void updateUser(User user) {
		if(this.userPersistance.readUser(user.getLoginname()) == null) {
			this.userPersistance.updateUser(user);
		}
	}

	/**
	 * L�scht den angegebenen Nutzer in der Datenbank
	 * @param user Der zu l�schende Benutzer
	 */
	public void deleteUser(User user) {
		if(this.userPersistance.readUser(user.getLoginname()) != null) {
			this.userPersistance.deleteUser(user);
		}
	}	

	/**
	 * liest einen User aus der Persistenzschicht und vergleicht das User-Passwort
	 * mit dem Parameter passwort
	 * 
	 * @param username
	 *            eindeutige Loginname
	 * @param passwort
	 *            das angegebene Passwort
	 * @return true, wenn das Passwort identisch ist, ansonsten false
	 */
	public boolean checkPassword(String username, String passwort) {
		User user = readUser(username, passwort);
		if (user != null)
			return user.getPassword().equals(passwort);
		else
			return false;
	}
}
